﻿$("#btnAddPerson").click(function () {
    $.post("/Home/AddPerson/", {
        name: $('#txtName').val(),
        phone: $('#txtPhone').val(),
        address: $('#txtAddress').val()
    }, 
    function(data, status){
        if (data.Status == true) {
            $('#records').append('<li><button id="' + data.PersonObject.Id + '" class="booking">Book</button> ' + data.PersonObject.Address + ' </li>');
            alert("Your record has been saved.");
        }
        else {
            alert(data.ErrorMessage);
        }
    }).
    error(function (data) {
        alert(data.responseText);
    });
});

$(".booking").click(function () {
    $.post("/Home/MakeBooking/", {
        personId: this.id
    }, 
    function(data, status){
        $('#output').html(data.Response);
    }).
    error(function(data) { 
        $('#output').html(data.responseText);
    });
});