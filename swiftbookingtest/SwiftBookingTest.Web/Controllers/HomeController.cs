﻿using SwiftBookingTest.Web.Business;
using SwiftBookingTest.Web.WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwiftBookingTest.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var dataService = new DataService();
            List<Person> persons = dataService.sqlServerClient.GetAllPeople();

            ViewBag.SavedPeople = persons;
            return View();
        }

        //
        // POST: /Home/AddPerson/
        [HttpPost]
        public ActionResult AddPerson(string name, string phone, string address)
        {
            Person person = new Person();
            person.Name = name.Trim();
            person.Phone = phone.Trim();
            person.Address = address.Trim();

            try
            {
                DataService dataService = new DataService();
                bool success = dataService.sqlServerClient.AddPerson(person);

                ResponseData responseData = new ResponseData();
                responseData.Status = success;
                responseData.PersonObject = person;
                return Content(responseData.ToJSON(), "application/json");
            }
            catch (Exception e)
            {
                ResponseData responseData = new ResponseData();
                responseData.Status = false;
                responseData.ErrorMessage = e.Message;
                return Content(responseData.ToJSON(), "application/json");
            }
        }

        //
        // POST: /Home/MakeBooking/
        [HttpPost]
        public ActionResult MakeBooking(int personId)
        {
            try
            {
                DataService dataService = new DataService();
                Person person = dataService.sqlServerClient.GetPerson(personId);

                if (person != null)
                {
                    string response = dataService.swiftDeliveryClient.MakeBooking(person);

                    ResponseData responseData = new ResponseData();
                    responseData.Status = true;
                    responseData.Response = response.ToString();
                    return Content(responseData.ToJSON(), "application/json");
                }
                else {
                    ResponseData responseData = new ResponseData();
                    responseData.Status = false;
                    responseData.Response = "Unable to find person. Your booking has not been organised.";
                    return Content(responseData.ToJSON(), "application/json");
                }
            }
            catch (Exception e)
            {
                ResponseData responseData = new ResponseData();
                responseData.Status = false;
                responseData.Response = e.Message;
                return Content(responseData.ToJSON(), "application/json");
            }
        }
    }
}
