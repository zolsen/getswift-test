﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.Business
{
    public class Person
    {
        public int Id;
        public string Name;
        public string Phone;
        public string Address;
    }
}