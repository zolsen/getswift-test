﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace SwiftBookingTest.Web.Business
{
    public class ResponseData
    {
        public bool Status;
        public string Response;
        public string ErrorMessage;
        public Person PersonObject;

        public string ToJSON()
        {
            var json = new JavaScriptSerializer().Serialize(this);
            return json;
        }
    }
}