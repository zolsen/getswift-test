﻿CREATE TABLE [dbo].[Booking] (
    [Id]      INT         IDENTITY (1, 1) NOT NULL,
    [name]    NCHAR (255) NULL,
    [phone]   NCHAR (255) NULL,
    [address] NCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

