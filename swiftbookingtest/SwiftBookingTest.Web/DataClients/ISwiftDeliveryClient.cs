﻿using SwiftBookingTest.Web.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwiftBookingTest.Web.DataClients
{
    public interface ISwiftDeliveryClient
    {
        string MakeBooking(Person person);
    }
}
