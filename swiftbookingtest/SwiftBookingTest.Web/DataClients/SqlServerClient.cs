﻿using SwiftBookingTest.Web.Business;
using SwiftBookingTest.Web.DataClients;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.WebService
{
    public class SqlServerClient : ISqlServerClient
    {
        private string CONNECTION_STRING = null;

        public SqlServerClient()
        {
            ConnectionStringSettings mySetting = ConfigurationManager.ConnectionStrings["DefaultConnection"];
            if (mySetting == null || string.IsNullOrEmpty(mySetting.ConnectionString))
                throw new Exception("Fatal error: missing connecting string in web.config file");
            CONNECTION_STRING = mySetting.ConnectionString;
        }

        public void CreateSqlDatabase(string filename)
        {
            try
            {
                string databaseName = System.IO.Path.GetFileNameWithoutExtension(filename);
                using (var connection = new System.Data.SqlClient.SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText =
                            String.Format("CREATE DATABASE {0} ON PRIMARY (NAME={0}, FILENAME='{1}')", databaseName, filename);
                        command.ExecuteNonQuery();

                        command.CommandText =
                            String.Format("EXEC sp_detach_db '{0}', 'true'", databaseName);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
	        {
                throw e;
            }
        }

        public bool AddPerson(Person person)
        {
            try
            {
                string cmdString = "INSERT INTO booking (name,phone,address) VALUES (@Name, @Phone, @Address); SELECT SCOPE_IDENTITY();";
                using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
                {
                    using (SqlCommand comm = new SqlCommand())
                    {
                        comm.Connection = conn;
                        comm.CommandText = cmdString;
                        comm.Parameters.AddWithValue("@Name", person.Name);
                        comm.Parameters.AddWithValue("@Phone", person.Phone);
                        comm.Parameters.AddWithValue("@Address", person.Address);

                        try
                        {
                            conn.Open();
                            comm.ExecuteNonQuery();

                            var rowCount = comm.ExecuteScalar();
                            person.Id = Convert.ToInt32(rowCount);

                            return true;
                        }
                        catch (SqlException e)
                        {
                            throw e;
                        }
                    }
                }
            }
            catch (Exception e)
	        {
                throw e;
            }
        }

        public List<Person> GetAllPeople()
        {
            List<Person> persons = new List<Person>();

            try
            {
                string cmdString = "SELECT * FROM booking";
                using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
                {
                    using (SqlCommand comm = new SqlCommand())
                    {
                        comm.Connection = conn;
                        comm.CommandText = cmdString;

                        try
                        {
                            conn.Open();

                            using (SqlDataReader reader = comm.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    Person person = new Person();
                                    person.Id = reader.GetInt32(0);
                                    person.Name = reader.GetString(1);
                                    person.Phone = reader.GetString(2);
                                    person.Address = reader.GetString(3);
                                    persons.Add(person);
                                }

                                return persons;
                            }
                        }
                        catch (SqlException e)
                        {
                            throw e;
                        }
                    }
                }
            }
            catch (Exception e)
	        {
                throw e;
            }
        }

        public Person GetPerson(int personId)
        {
            try
            {
                string cmdString = "SELECT * FROM booking WHERE id = " + personId;
                using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
                {
                    using (SqlCommand comm = new SqlCommand())
                    {
                        comm.Connection = conn;
                        comm.CommandText = cmdString;

                        try
                        {
                            conn.Open();

                            using (SqlDataReader reader = comm.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    Person person = new Person();
                                    person.Id = reader.GetInt32(0);
                                    person.Name = reader.GetString(1);
                                    person.Phone = reader.GetString(2);
                                    person.Address = reader.GetString(3);
                                    return person;
                                }

                                return null;
                            }
                        }
                        catch (SqlException e)
                        {
                            throw e;
                        }
                    }
                }
            }
            catch (Exception e)
	        {
                throw e;
            }
        }
    }
}