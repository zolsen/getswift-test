﻿using SwiftBookingTest.Web.DataClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.WebService
{
    public class DataService
    {
        public ISqlServerClient sqlServerClient;
        public ISwiftDeliveryClient swiftDeliveryClient;

        public DataService()
        {
            sqlServerClient = new SqlServerClient();
            swiftDeliveryClient = new SwiftDeliveryClient();
        }
    }
}