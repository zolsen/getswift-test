﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SwiftBookingTest.Web.Business;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Text;
using System.Net;

namespace SwiftBookingTest.Web.DataClients
{
    public class SwiftDeliveryClient : ISwiftDeliveryClient
    {
        private const string API_URL = "http://app.getswift.co/api/v2/deliveries";
        private const string MERCHANT_KEY = "3285db46-93d9-4c10-a708-c2795ae7872d";

        private const string BOOK_A_DELIVERY = "Book_A_Delivery.json";

        public string MakeBooking(Person person)
        {
            string jsonPacket = ReadRequestPacket(BOOK_A_DELIVERY);
            jsonPacket = jsonPacket.Replace("MERCHANT_KEY", MERCHANT_KEY);
            jsonPacket = jsonPacket.Replace("PICKUP_NAME", "Zaidon");
            jsonPacket = jsonPacket.Replace("PICKUP_PHONE", "1234567890");
            jsonPacket = jsonPacket.Replace("PICKUP_ADDRESS", "3 High St, Reservoir, 3073");
            jsonPacket = jsonPacket.Replace("DROPOFF_NAME", person.Name);
            jsonPacket = jsonPacket.Replace("DROPOFF_PHONE", person.Phone);
            jsonPacket = jsonPacket.Replace("DROPOFF_ADDRESS", person.Address);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(API_URL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json; charset=utf-8";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonPacket);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    return result.ToString();
                }
            }
        }

        private string ReadRequestPacket(string packetName)
        {
            string text;
            var fileStream = new FileStream(HttpContext.Current.ApplicationInstance.Server.MapPath("~/WebServiceRequestPackets") + "/" + packetName, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                text = streamReader.ReadToEnd();
            }
            return text;
        }
    }
}